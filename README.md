# Docker Ubuntu with OpenSSH-Server

## **0. 更新说明**
> **ilemonrain/ubuntu-sshd** Build 20171125
> ```
> [New] 增加 Ubuntu 18.04 Devel （开发版，建议不要用在生产环境中，小白鼠随意）    
> [New] 补全版本，从12.04到18.04 （太远古的版本不推荐使用）    
> [Change] BitBucket端重新安排了目录结构，开发&使用更简单清晰     
> ```

## **1. 镜像各个版本及Dockerfile**
> Ubuntu 18.04 Devel (ubuntu-sshd:18.04)  [Dockerfile](https://bitbucket.org/ilemonrain/docker-ilemonrain-ubuntu-sshd/src/fa4d7fb93bd7d0a0497d2f6a87273cf2a884acb7/ubuntu-18.04/Dockerfile?at=master&fileviewer=file-view-default)    
> Ubuntu 17.10 (ubuntu-sshd:17.10)  [Dockerfile](https://bitbucket.org/ilemonrain/docker-ilemonrain-ubuntu-sshd/src/fa4d7fb93bd7d0a0497d2f6a87273cf2a884acb7/ubuntu-17.10/Dockerfile?at=master&fileviewer=file-view-default)           
> Ubuntu 17.04 (ubuntu-sshd:17.04)  [Dockerfile](https://bitbucket.org/ilemonrain/docker-ilemonrain-ubuntu-sshd/src/fa4d7fb93bd7d0a0497d2f6a87273cf2a884acb7/ubuntu-17.10/Dockerfile?at=master&fileviewer=file-view-default)            
> Ubuntu 16.10 (ubuntu-sshd:16.10)  [Dockerfile](https://bitbucket.org/ilemonrain/docker-ilemonrain-ubuntu-sshd/src/fa4d7fb93bd7d0a0497d2f6a87273cf2a884acb7/ubuntu-16.10/Dockerfile?at=master&fileviewer=file-view-default)         
> Ubuntu 16.04 (ubuntu-sshd:16.04 , ubuntu-sshd:latest)  [Dockerfile](https://bitbucket.org/ilemonrain/docker-ilemonrain-ubuntu-sshd/src/fa4d7fb93bd7d0a0497d2f6a87273cf2a884acb7/ubuntu-16.04/Dockerfile?at=master&fileviewer=file-view-default)      
> Ubuntu 14.10 (ubuntu-sshd:14.10)  [Dockerfile](https://bitbucket.org/ilemonrain/docker-ilemonrain-ubuntu-sshd/src/fa4d7fb93bd7d0a0497d2f6a87273cf2a884acb7/ubuntu-14.10/Dockerfile?at=master&fileviewer=file-view-default)         
> Ubuntu 14.04 (ubuntu-sshd:14.04)  [Dockerfile](https://bitbucket.org/ilemonrain/docker-ilemonrain-ubuntu-sshd/src/fa4d7fb93bd7d0a0497d2f6a87273cf2a884acb7/ubuntu-14.04/Dockerfile?at=master&fileviewer=file-view-default)         
> Ubuntu 12.10 (ubuntu-sshd:12.10)  [Dockerfile](https://bitbucket.org/ilemonrain/docker-ilemonrain-ubuntu-sshd/src/fa4d7fb93bd7d0a0497d2f6a87273cf2a884acb7/ubuntu-12.10/Dockerfile?at=master&fileviewer=file-view-default)         
> Ubuntu 12.04 (ubuntu-sshd:12.04)  [Dockerfile](https://bitbucket.org/ilemonrain/docker-ilemonrain-ubuntu-sshd/src/fa4d7fb93bd7d0a0497d2f6a87273cf2a884acb7/ubuntu-12.04/Dockerfile?at=master&fileviewer=file-view-default)         

## **2. 镜像说明**
>本镜像基于 [Ubuntu](https://hub.docker.com/_/ubuntu/) 官方Docker镜像制作而成，Dockerfile制作思路参考 [rastasheep/ubuntu-sshd](https://hub.docker.com/r/rastasheep/ubuntu-sshd/) 制作。原则上，建议运行此Docker镜像的内存空间**不低于256MB**，以保证流畅的SSH连接以及运行的稳定性。实在无法获得大于256MB空间的Docker容器，请移步使用 [ilemonrain/alpine-sshd](https://hub.docker.com/r/ilemonrain/alpine-sshd/)。

## **3. 使用说明**
> 拉取镜像：**docker pull ilemonrain/ubuntu-sshd:latest**  
> 使用镜像：**docker run -d -p 22:22
--name=ilemonrain/alpine-sshd lemonrain/alpine-sshd:latest**  
> - **参数基础格式**：docker run -d **-p 22:22** **-name=ilemonrain/alpine-sshd** ilemonrain/alpine-sshd:latest  
> - **-p 22:22**：端口映射，冒号前面的是外部端口，冒号后面的为内部端口，根据自己需要自行部署（比如要部署个Nginx，那就是“**-p 22:22 -p 80:80**”）    
> - **--name=ilemonrain/alpine-ssh**：容器名称，自行确定，也可以省略（但不方便以后管理）    
> - **默认SSH信息：**    **用户名：root    密码：ubuntu**  
>   
> - **！！！部署成功后立刻改掉ROOT密码！！！**
> - **！！！部署成功后立刻改掉ROOT密码！！！**
> - **！！！部署成功后立刻改掉ROOT密码！！！**
> - **！！！重要的事情说三遍！！！**
> - **！！！因不改ROOT密码导致容器出现问题，概不负责！！！**
> - 参数(env)修改密码功能正在开发中……
>

## **4. 联系作者**
>Email 到：[ilemonrain@ilemonrain.com](mailto:ilemonrain@ilemonrain.com)
